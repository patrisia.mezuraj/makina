from django.db import models
from django.db.models.query import NamedValuesListIterable

class Vehicle(models.Model):
    AUTOMATIC= "Automatic"
    MANUAL="Manual"
    TRANSMISSION_CHOICES=[
    (AUTOMATIC, AUTOMATIC),
    (MANUAL, MANUAL),
    ]
    brand = models.CharField(max_length=100, null=False)
    model = models.CharField(max_length=100, null=False)
    year=models.PositiveBigIntegerField(null=False)
    transmission=models.CharField(max_length=32, null=True, choices=TRANSMISSION_CHOICES)

    def __str__(self) -> str:
        return f'{self.brand} {self.model}'